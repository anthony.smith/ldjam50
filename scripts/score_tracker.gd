extends Node

var dir = Directory.new()
var dirOpen = false
var score = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	var err = dir.open("user://")
	if (0 != err):
		print("Failed to open user directory. Error #: ", err)
	dirOpen = (0 != err)
	readHighScore()
	pass
	
func updateLabel():
	$".".set_text("High Score: " + String(score))

func readHighScore():
	if (dir.file_exists("highscore.sav")):
		var file = File.new()
		file.open(dir.get_current_dir() + "highscore.sav", File.READ)
		var value = file.get_as_text()
		file.close()
		score = int(value)
		updateLabel()
	pass
	
func saveHighScore(newScore):
	if (newScore > score):
		var file = File.new()
		file.open(dir.get_current_dir() + "highscore.sav", File.WRITE)
		file.store_string(String(newScore))
		file.close()
		score = newScore
		updateLabel()
	pass
