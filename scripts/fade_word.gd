extends Node

class_name FadeWord

var me = self

# Called when the node enters the scene tree for the first time.
func _ready():
	me.modulate.a = 0;
	pass

func MakeVisible(word):
	me.set_text(word)
	var tween = get_node("Tween")
	tween.interpolate_property(me, "modulate", Color(1,1,1,1), Color(1,1,1,0), 3, Tween.TRANS_LINEAR, Tween.EASE_OUT, 0)
	tween.start()
	playAudio()
	
func playAudio():
	$Completed.play()
	pass
