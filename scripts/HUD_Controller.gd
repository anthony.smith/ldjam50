extends CanvasLayer

signal start

# Called when the node enters the scene tree for the first time.
func _ready():	
	pass
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func update_timer_text(time):
	$Camera2D/TimeRemainingLabel.text = "TIME:" + String(time)

func update_score_text(text):
	$Camera2D/ScoreLabel.text = "Words Guessed: " + String(text)
	$Camera2D/HighScore.saveHighScore(text)

func update_tries_remaining(tries):
	$Camera2D/TriesRemainingLabel.text = String(tries) + " tries remaining."

func update_guess_word_text(text):
	$Camera2D/GuessWordLabel.text = text

func _on_StartButton_pressed():
	$Camera2D/StartButton.hide()
	emit_signal("start")
	$Camera2D/Start.play()
	update_score_text(0)

func show_start_button():
	$Camera2D/StartButton.show()
	
func word_correct(word):
	$Camera2D/CorrectlyGuessed.MakeVisible(word)
