class_name Vocabulary

var fileName = "res://dictionary.txt"
var dictionary = []
var rand = RandomNumberGenerator.new()

func _init():
	var file = File.new()
	file.open(fileName, File.READ)
	var text = file.get_as_text()
	text = text.split("\n")
	for word in text:
		dictionary.append(word.strip_edges())
	file.close()	
	rand.randomize()
################################################################################
# Gets the next random word in the dictionary.
#
# Returns a word.
#
func NextWord():
	var next = rand.randi_range(0, len(dictionary))
	return dictionary[next]
