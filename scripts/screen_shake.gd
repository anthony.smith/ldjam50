extends Node2D

class_name ScreenShake

var position_changes_x = 5;
var original_pos

# Called when the node enters the scene tree for the first time.
func _ready():
	original_pos = get_position()
	print("Original Pos: ", original_pos)
	pass

func shake():
	start_shake()
	
func on_tween_completed():
	print("on_tween_completed")
	print("Current Pos: ", get_position())
	var from_pos = get_position();
	var target_pos = original_pos
	var tween = get_node("CameraTween")
	tween.interpolate_property(self, "position", from_pos, target_pos, 0.1)
	tween.disconnect("tween_all_completed", self, "on_tween_completed")
	tween.start()

func start_shake():
	var tween = get_node("CameraTween")
	var target_pos = Vector2(original_pos.x, original_pos.y + position_changes_x)
	tween.interpolate_property(self, "position", original_pos, target_pos, 0.1)
	var res = tween.connect("tween_all_completed", self, "on_tween_completed")
	print("Connected: ", res)
	tween.start()
