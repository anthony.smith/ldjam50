extends Node

var score
var time
var misses
var timer
var onesecond = 0
var currentword = ""
var wordsdict
var hiddenword = ""
# Called when the node enters the scene tree for the first time.
func _ready():
	#new_game()
	pass

#Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	onesecond += delta
	if (1 < onesecond):
		onesecond = 0
		$HUD.update_timer_text(round($CountdownTimer.time_left))

func _init():
	score = 0
	time = 60
	misses = 10
	wordsdict = Vocabulary.new()	

func _input(event):
	if event is InputEventKey and event.pressed:
		process_letter_input(event.as_text().to_lower())
		if (misses <= 0):
			misses = 10
			new_word()
		$HUD.update_tries_remaining(misses)

func game_over():
	$CountdownTimer.stop()
	$HUD.show_start_button()

func new_game():
	print(time)	
	print(score)
	$HUD.update_score_text(score)
	new_word()
	$HUD.update_timer_text(time)
	$CountdownTimer.start(time)

func _on_CountdownTimer_timeout():
	print("timer has expired")
	game_over()

func update_countdown_timer(time):
	var timeleft = $CountdownTimer.time_left
	$CountdownTimer.stop()
	$CountdownTimer.start(timeleft+time)

func process_letter_input(letter):
	if ($CountdownTimer.time_left <= 0):
		return
	var letterPresent = false
	var letterWasPresent = false
	var i = 0
	while i < currentword.length():		
		letterPresent = false
		if (currentword[i] == letter):
			letterPresent = true
			letterWasPresent = true
		if (hiddenword[i] != letter && letterPresent == true):
			hiddenword[i] = letter
		i += 1
	if letterWasPresent == true:
		$HUD.update_guess_word_text(hiddenword)
	if letterWasPresent == false:
		misses -= 1
		$HUD/Camera2D.shake()
		$HUD/Camera2D/Miss.play()
	if (currentword == hiddenword):
		$HUD.word_correct(hiddenword)
		score +=1
		$HUD.update_score_text(score)
		misses = 10
		$HUD.update_tries_remaining(misses)
		update_countdown_timer(5)
		new_word()

func _on_HUD_start():
	new_game()

func new_word():
	currentword = wordsdict.NextWord()
	hiddenword = currentword
	var i = 0
	while i < hiddenword.length():
		hiddenword[i] = "-"
		i += 1
	print(hiddenword)
	print(currentword)
	$HUD.update_guess_word_text(hiddenword)
