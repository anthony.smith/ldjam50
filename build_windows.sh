#!/usr/bin/env bash

DIR=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)
TEMPLATE_DIR=/root/.local/share/godot/templates/3.4.4.stable/

mkdir -p $TEMPLATE_DIR

mv godot_templates.zip $TEMPLATE_DIR

cd $TEMPLATE_DIR
unzip godot_templates.zip
rm godot_templates.zip
cd $DIR

godot --export "Windows_Desktop" ./wordable.exe
